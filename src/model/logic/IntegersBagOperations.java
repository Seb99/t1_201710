package model.logic;

import java.util.Iterator;

import javafx.beans.binding.IntegerBinding;
import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	//---------------------------------MIS METODOS-----------------------------------------
	
	
	public int getMin(IntegersBag bag)
	{
		int min = 999999999;
		int valor;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				valor = iter.next();
				if(min > valor)
				{
					min = valor;
				}
			}
		}	
		return min;
	}
	
	
	public int getMaxPar(IntegersBag bag)
	{
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if( max < value && value%2 == 0 )
				{					
						max = value;										
				}
			}			
		}
		return max;
	}
	
	
	public int getMaxImpar(IntegersBag bag)
	{
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if( max < value && value%2 != 0 )
				{					
						max = value;										
				}
			}			
		}
		return max;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
